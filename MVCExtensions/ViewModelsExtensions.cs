﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;

namespace MVCExtensions
{
    public static class ViewModelsExtensions
    {

        public static T ThrowNotFoundIfNull<T>(this T viewModel)
        {
            if (viewModel == null)
            {
                throw new HttpException(404, "Resource not found!");
            }

            return viewModel;
        }

    }
}
