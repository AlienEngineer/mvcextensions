﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Reflection;
using System.Text;

namespace MVCExtensions
{
    public class Http : HttpClient
    {

        public String Host { get; private set; }

        public Http(String host)
        {
            Host = host;
        }

        public HttpResponseMessage Get(String pathMask, params object[] args)
        {
            return GetAsync(Host + String.Format(pathMask, args)).Result;
        }

        public HttpResponseMessage Post<TModel>(TModel model, String pathMask, params object[] args)
        {
            return PostAsync(
                Host + String.Format(pathMask, args),
                new FormUrlEncodedContent(ConvertToDictionary(model))
                ).Result;
        }

        private static String GetValue<TModel>(PropertyInfo prop, TModel model)
        {
            object value = prop.GetValue(model, null);
            return value != null ? value.ToString() : null;
        }

        private static IEnumerable<KeyValuePair<string, string>> ConvertToDictionary<TModel>(TModel model)
        {
            return typeof(TModel).GetProperties()
                .Select(prop =>
                    new KeyValuePair<String, String>(prop.Name, GetValue<TModel>(prop, model)));

        }

    }
}
