﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Mvc;

namespace MVCExtensions
{

    public class MvcController : Controller
    {

        private StatusCodes _StatusCode;
        
        /// <summary>
        /// Gets or sets the status code.
        /// </summary>
        /// <value>
        /// The status code.
        /// </value>
        protected StatusCodes StatusCode
        {
            get
            {
                return _StatusCode;
            }
            set
            {
                Response.StatusCode = (int)value;
                _StatusCode = value;
            }
        }

        /// <summary>
        /// Called when [exception].
        /// </summary>
        /// <typeparam name="TModel">The type of the model.</typeparam>
        /// <param name="viewName">Name of the view.</param>
        /// <param name="modelGetter">The model getter.</param>
        protected void OnException<TModel>(String viewName, Func<TModel> modelGetter)
        {
            ModelGetter = modelGetter;
            ViewName = viewName;
        }

        protected void OnException<TModel>(Func<TModel> modelGetter)
        {
            ModelGetter = modelGetter;
        }

        public Object ModelGetter { get; set; }
        public String ViewName { get; set; }

    }
}
