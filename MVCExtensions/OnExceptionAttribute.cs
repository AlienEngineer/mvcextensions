using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Mvc;

namespace MVCExtensions
{
    /// <summary>
    /// The model is found by name convention. It should end with Model word.
    /// </summary>
    public class OnExceptionAttribute : ActionFilterAttribute
    {
        private readonly String _ViewName;
        
        /// <summary>
        /// Initializes a new instance of the <see cref="OnExceptionAttribute" /> class.
        /// </summary>
        /// <param name="viewName">Name of the view.</param>
        public OnExceptionAttribute(String viewName)
        {
            _ViewName = viewName;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="OnExceptionAttribute" /> class.
        /// </summary>
        public OnExceptionAttribute() { }

        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {

            filterContext.Controller.TempData["viewName"] = _ViewName ?? filterContext.ActionDescriptor.ActionName;
            filterContext.Controller.TempData["model"] = GetModelGetter(filterContext);
            
            base.OnActionExecuting(filterContext);
        }

        private static Func<Object> GetModelGetter(ActionExecutingContext filterContext)
        {
            return () => filterContext.ActionParameters.Values
                .FirstOrDefault(val => val.GetType().Name.EndsWith("Model"));
        }

    }
}
