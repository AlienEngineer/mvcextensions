﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Mvc;

namespace MVCExtensions.Filtering
{
    /// <summary>
    /// Exception Filter to handle Business exceptions.
    /// </summary>
    /// <typeparam name="TException">The type of the exception.</typeparam>
    public abstract class ExceptionFilter<TException> : IExceptionFilter
    {

        private ExceptionContext _FilterContext;
        private readonly IDictionary<String, String> errorMessages = new Dictionary<String, String>();
        private readonly IEnumerable<IErrorMessageHandler> _ErrorMessageHandlers;

        public ExceptionFilter(params IErrorMessageHandler[] errorMessageHandlers)
        {
            _ErrorMessageHandlers = errorMessageHandlers;
        }

        public void OnException(ExceptionContext filterContext)
        {
            if (filterContext.ExceptionHandled)
            {
                return;
            }

            _FilterContext = filterContext;
            if (typeof(TException).IsAssignableFrom(filterContext.Exception.GetType()))
            {
                Handle(filterContext);
                
                /* Clean up */
                errorMessages.Clear();
            }
        }

        /// <summary>
        /// Renders the view.
        /// </summary>
        /// <param name="viewName">Name of the view.</param>
        /// <param name="model">The model.</param>
        protected void RenderView(String viewName, Object model)
        {
            var view = new ViewResult() { ViewName = viewName };
            view.ViewData.Model = model;

            foreach (var item in errorMessages)
            {
                view.ViewData.ModelState.AddModelError(item.Key, item.Value);
            }

            view.ExecuteResult(_FilterContext.Controller.ControllerContext);
            _FilterContext.ExceptionHandled = true;
        }

        /// <summary>
        /// Sets the status code.
        /// </summary>
        /// <param name="statusCode">The status code.</param>
        protected void SetStatusCode(StatusCodes statusCode)
        {
            _FilterContext.HttpContext.Response.StatusCode = (int)statusCode;
        }
        
        /// <summary>
        /// Decodes the message.
        /// </summary>
        /// <param name="exception">The exception.</param>
        /// <returns></returns>
        protected string DecodeMessage(Exception exception)
        {

            foreach (var handler in _ErrorMessageHandlers)
            {
                if (handler.CanHandle(exception))
                {
                    return handler.Handle(exception);
                }
            }

            return exception.Message;
        }

        /// <summary>
        /// Adds the error message.
        /// </summary>
        /// <param name="message">The message.</param>
        /// <param name="args">The args.</param>
        protected void AddErrorMessage(String message, params Object[] args)
        {
            AddErrorMessage("", message, args);
        }

        /// <summary>
        /// Adds the error message.
        /// </summary>
        /// <param name="field">The field.</param>
        /// <param name="message">The message.</param>
        /// <param name="args">The args.</param>
        protected void AddErrorMessage(String field, String message, params Object[] args)
        {
            errorMessages[field] = String.Format(message, args);
        }

        /// <summary>
        /// Handles the specified exception.
        /// </summary>
        /// <param name="filterContext">The filter context.</param>
        protected abstract void Handle(ExceptionContext filterContext);

    }
}
