using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Mvc;

namespace MVCExtensions.Filtering
{
    public sealed class ControllerBaseExceptionCatcher<TException> : ExceptionFilter<TException>
    {

        public ControllerBaseExceptionCatcher(params IErrorMessageHandler[] errorMessageHandlers)
            : base(errorMessageHandlers)
        { }

        /// <summary>
        /// Handles the specified filter context.
        /// </summary>
        /// <param name="filterContext">The filter context.</param>
        protected override void Handle(ExceptionContext filterContext)
        {
            var controller = filterContext.Controller as MvcController;

            if (controller == null)
            {
                return;
            }

            if (String.IsNullOrEmpty(controller.ViewName))
            {
                return;
            }

            AddErrorMessage(DecodeMessage(filterContext.Exception));
            if (!filterContext.RequestContext.HttpContext.Request.Browser.IsMobileDevice)
            {
                SetStatusCode(StatusCodes.BadRequest);
            }
            try
            {
                /* TempData convention using the ControllerBase.OnException Method... */
                RenderView(controller.ViewName, ((Delegate)controller.ModelGetter).DynamicInvoke());
            }
            catch (Exception ex)
            {
                throw new InvalidProgramException("N�o foi poss�vel receber uma refer�ncia para o modelo.", ex);
            }

        }
    }
}
