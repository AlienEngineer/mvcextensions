using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Mvc;

namespace MVCExtensions
{
    public enum StatusCodes
    {
        Created = 201,
        BadRequest = 400,
        Unauthorized = 401,
        Forbiben = 403,
        NotFound = 404
    }
}
