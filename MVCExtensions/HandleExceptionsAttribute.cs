using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Mvc;

namespace MVCExtensions
{
    /// <summary>
    /// The model is found by name convention. It should end with Model word.
    /// </summary>
    public class HandleExceptionsAttribute : ActionFilterAttribute
    {
        protected readonly String _ViewName;

        /// <summary>
        /// Initializes a new instance of the <see cref="OnExceptionAttribute" /> class.
        /// </summary>
        /// <param name="viewName">Name of the view.</param>
        public HandleExceptionsAttribute(String viewName)
        {
            _ViewName = viewName;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="OnExceptionAttribute" /> class.
        /// </summary>
        public HandleExceptionsAttribute() { }

        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            var controller = filterContext.Controller as MvcController;
            if (controller != null)
            {
                controller.ViewName = ViewName ?? filterContext.ActionDescriptor.ActionName;
                controller.ModelGetter = GetModelGetter(filterContext);
            }

            base.OnActionExecuting(filterContext);
        }

        public virtual Func<Object> GetModelGetter(ActionExecutingContext filterContext)
        {
            return () => filterContext.ActionParameters.Values
                .FirstOrDefault(val => val.GetType().Name.EndsWith("Model"));
        }

        public String ViewName
        {
            get
            {
                return _ViewName;
            }
        }

    }
}
