using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Mvc;

namespace MVCExtensions
{
    public class AutoHandleExceptionsAttribute : HandleExceptionsAttribute
    {

        /// <summary>
        /// Initializes a new instance of the <see cref="OnExceptionAttribute"/> class.
        /// </summary>
        /// <param name="viewName">Name of the view.</param>
        public AutoHandleExceptionsAttribute(String viewName)
            : base(viewName)
        {

        }

        public override Func<Object> GetModelGetter(ActionExecutingContext filterContext)
        {

            var mi = filterContext.Controller.GetType().GetMethods(System.Reflection.BindingFlags.Instance | System.Reflection.BindingFlags.NonPublic)
                .FirstOrDefault(m =>
                    m.GetCustomAttributes(typeof(AutoHandleExceptionsAttribute), true)
                        .Cast<AutoHandleExceptionsAttribute>()
                        .Count(a => a.ViewName.Equals(ViewName)) > 0);

            if (mi != null)
            {
                return () => mi.Invoke(
                    filterContext.Controller,
                    new Object[] { base.GetModelGetter(filterContext).Invoke() });
            }

            return () => null;
        }

    }
}
