﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MVCExtensions
{
    /// <summary>
    /// Represents a Message Handler.
    /// </summary>
    public interface IErrorMessageHandler
    {
        /// <summary>
        /// Determines whether this instance can handle the specified exception.
        /// </summary>
        /// <param name="exception">The exception.</param>
        /// <returns></returns>
        Boolean CanHandle(Exception exception);

        /// <summary>
        /// Handles the specified exception.
        /// </summary>
        /// <param name="exception">The exception.</param>
        /// <returns>A new message...</returns>
        String Handle(Exception exception);


    }
}
